---
layout: handbook-page-toc
title: UX Department Learning and Development
description: >-
  This page contains links to internal and external resources that members of the UX Department at GitLab can use to build their skills.
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Welcome to Learning and Development for the UX Department at GitLab! You are welcome to explore content here for now. We'll update this page when we add this content to GitLab Learn.

The resources in this page are meant to support product designers, researchers, technical writers, and their managers to explore, learn and grow at their own pace. We aim to collect content that spans various skill levels, as well as various levels of depth/commitment. It is recommended that the UX Department engage with the resources here to help them have a successful journey at GitLab and in their career as a whole. 

Most of the resources here are free but any content requiring payment [can be reimbursed following the GitLab reimbursement policies](/handbook/spending-company-money/).

## Recommended resources for all UX Department

### Product design

#### Books
- [The Design of Everyday Things](https://www.amazon.com/Design-Everyday-Things-Revised-Expanded/dp/0465050654) by Don Norman

#### LinkedIn Learning
- [Making User Experience Happen as a Team](https://www.linkedin.com/learning/making-user-experience-happen-as-a-team)

### Design strategy

#### Books
- [Hacking Growth: How Today's Fastest-Growing Companies Drive Breakout Success](https://www.amazon.com/Hacking-Growth-Fastest-Growing-Companies-Breakout/dp/045149721X) by Sean Ellis
- [Better Onboarding](https://abookapart.com/products/better-onboarding) by Krystal Higgins

#### Videos
- [Design Complex Applications: A Framework](https://www.nngroup.com/videos/designing-complex-apps-framework/?utm_source=Alertbox&utm_campaign=1651149249-EMAIL_CAMPAIGN_2020_11_12_08_52_COPY_01&utm_medium=email&utm_term=0_7f29a2b335-1651149249-40578453)

### Design systems

#### Books
- [Building Design Systems: Unify User Experiences through a Shared Design Language](https://www.amazon.com/Building-Design-Systems-Experiences-Language/dp/148424513X) by Sarrah Vesselov, Taurie Davis
- [Expressive Design Systems](https://abookapart.com/products/expressive-design-systems) by Yesenia Perez-Cruz

### UX Research

#### Handbook Links
- [Discovery Resources for Product Managers](/handbook/product/product-manager-role/learning-and-development)

### Jobs to be Done

#### Handbook Links
- [JTBD Resources for Product Managers](/handbook/product/product-manager-role/learning-and-development)

### Books
- [The Jobs to be Done Playbook](https://www.goodreads.com/book/show/52105688-the-jobs-to-be-done-playbook) by Jim Kalbach

### UX writing

- [Microcopy: Discover How Tiny Bits of Text Make Tasty Apps and Websites](https://www.amazon.com/dp/B071S54VLL/ref=cm_sw_em_r_mt_dp_51MKD8EZ2M7KMZYX7N2T) by Niaw de Leon

### Leadership and Communication

#### Books
- [Radical Candor](https://www.amazon.com/Radical-Candor-Kim-Scott/dp/B01KTIEFEE) by Kim Scott
- [Articulating Design Decisions](https://www.goodreads.com/book/show/25520974-articulating-design-decisions) by Tom Greever

#### Handbook Links

- [Leadership at GitLab: Recommended articles](/handbook/leadership/#articles)
- [Leadership at GitLab: Recommended books](/handbook/leadership/#books)

#### LinkedIn Learning
- [Leading with Intelligent Disobediance](https://www.linkedin.com/learning/leading-with-intelligent-disobedience/what-is-intelligent-disobedience?u=2255073)

### Time management and productivity

#### Books
- [Deep Work - Rules for Focused Success in a Distracted World](https://www.amazon.com/dp/B013UWFM52/ref=cm_sw_r_tw_dp_F00CBYJ3VJMPM6A2NYNQ) by Cal Newport

## Other GitLab resources

-[GitLab Book Clubs project](https://gitlab.com/gitlab-com/book-clubs)
- [Product Management Learning and Development](/handbook/product/product-manager-role/learning-and-development/) - a comprehensive list of internal and external resources that Product Managers at GitLab can use to build their skills.
- [Leadership Book clubs](/handbook/leadership/book-clubs/)

## UX Book Club

From time to time, we run an internal book club on a book from one of our resource lists. Join the conversation in `#ux-book-club`.
